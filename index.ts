console.log("Starting")
import express from "express";
const app = express();
const port = 443;
const fs = require("fs");
var https = require("https");
var http = require("http");
var privateKey = fs.readFileSync("secrets/id_rsa", "utf8");
var certificate = fs.readFileSync("secrets/cacert.pem", "utf8");
var credentials = { key: privateKey, cert: certificate };

app.get("/", (req, res) => {
  res.send("\nHello World!\n");
});

var httpsServer = https.createServer(credentials, app);
var httpServer = http.createServer(app);
httpsServer.listen(port, () => {
  console.log(`Server running at https://localhost/`);
});
console.log("Done")
